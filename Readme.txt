This repository contains the supplementary material of the following paper:

Yi Hong, Nikhil Singh, Roland Kwitt, and Marc Niethammer. 
"Time-warped Geodesic Regression". MICCAI 2014, Boston.